//Теоретичні питання
/*1. Опишіть, як можна створити новий HTML тег на сторінці.
Щоб створити новий тег на сторінці потрібно використати метод insertAdjacentHTML(a, b), де а - місце вставки, b - сам елемент. Наприклад, так ми додамо елемент <div> одразу після обраного елемента element:
element.insertAdjacentHTML("afterend", "<div>");.

Можемо параметру b надати значення, наприклад, так:
let newElement = "<div>Hello!</div>";
element.insertAdjacentHTML("afterend", newElement);

Також можна працювати з вузлами:
- створити вузол, використовуючи метод createElement();
- додати на сторінку за допомогою методу вставки вузла. В нашому випадку:
element.after(document.createElement("div"));.
На сторінці під вузлом з'явиться: <div></div>.

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції визначає місце розташування елемента HTML, що створюється. Варіанти значень 4:
- "beforebegin" - перед елементом;
- "afterbegin" - всередині елемента на його початку;
- "beforeend" - всередині елемента на його кінці;
- "afterend" - одразу після елемента.

3. Як можна видалити елемент зі сторінки?
Елемент зі сторінки можна видалити використанням методу remove().
Також елемент видаляють зі свого старого місця всі варіанти вставки вузлів (не зі сторінки, але місце змінюється).*/

// Практичне завдання
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

function getList(arr, parent = document.body) {
  arr = arr.reverse(arr);
  let createMainElement = document.createElement("ul");
  parent.prepend(createMainElement);

  for (let i of arr) {
    let elemList = `<li>${i}</li>`;
    createMainElement.insertAdjacentHTML("afterbegin", elemList);
  }
}

const someArray = ["Kharkiv", "Kiev", "Odessa", "Lviv", "Dnieper"];
let element;
getList(someArray, element);
